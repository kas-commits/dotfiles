<img src="https://user-images.githubusercontent.com/1292576/190241835-41469235-f65d-4d4b-9760-372cdff7a70f.png" width="48">

# Dotfiles
![GitHub last commit](https://img.shields.io/gitlab/last-commit/kas-commits/dotfiles?style=plastic)

These dotfiles are now managed by Nix. Specifically, the majority of the
development environment is handled by home-manager. There is additional
configuration for darwin-based systems. In a future commit I will also add
NixOS support.

## Prerequisites

### Ensure Nix is installed

For operating systems other than NixOS you have to manually install the Nix
ecosystem:

```sh
sh <(curl -L https://nixos.org/nix/install)
```

This will install Nix for both Linux systems and MacOS systems.

### Allow Flakes Experimental Features

You can do this by running the following:

```sh
echo "experimental-features = nix-command flakes" >> ~/.config/nix/nix.conf
```

or, more preferably, you should include this configuration option at the CLI
instead:

```sh
nix --experimental-features "nix-command flakes" ...
```

## Building & Installing

For all build and install options, you can either download this repo locally or
directly build the repo by changing the URI. The current

```sh
export FLAKE_URI="gitlab:kas-commits/dotfiles"
```

otherwise if you download the repo locally you can just set the following

```sh
export FLAKE_URI=.
```

### Option 1: Using the Default System Package

This repo's flake includes a default package for the supported systems. To
build the default output you can run:

```sh
nix build $FLAKE_URI
```

Followed by

```sh
result/activate
```

### Option 2: Only Install Home-Manager

If you only want the Home Manager configuration you can make sure to pin the
build output directly by running this command:

```sh
nix build "${FLAKE_URI}#homeConfigurations.${USER}@$(hostname).activationPackage"
```

and like Option 1 you have to run the activation script afterwards

```sh
result/activate
```

## Post-Install

### Uninstalling Root's version of nix in favor of nix-darwin

If you decide to use nix-darwin ***and also*** have it manage/update nix then
try uninstalling nix from the root user:

```sh
sudo -i nix-env -e nix
```

You can make sure this worked by runing this command before and after
uninstalling:

```sh
nix doctor
```

### Updating

This repo stores a file called `flake.lock`. This lock file locks all the
packages to specific versions at the time of building. This lets us have
perfectly reproducable environments & builds on a commit-level. If you want to
upgrade the packages to new versions, you first have to run:

```sh
nix flake update  # Make sure you're in the repo root
```

this will update the lock file. After which you can update the packages managed
by home-manager as usual by switching to the new generation:

```sh
home-manager switch --refresh --flake .
```
