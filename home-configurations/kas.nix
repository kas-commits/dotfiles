{
  # inputs,
  config,
  lib,
  pkgs,
  ...
}: let
  username = "kas";
  homeDirectory = "/home/${username}";
  colors = {
    Catppuccin = {
      Macchiato = {
        rgb = {
          Rosewater = "244, 219, 214";
          Flamingo = "240, 198, 198";
          Pink = "245, 189, 230";
          Mauve = "198, 160, 246";
          Red = "237, 135, 150";
          Maroon = "238, 153, 160";
          Peach = "245, 169, 127";
          Yellow = "238, 212, 159";
          Green = "166, 218, 149";
          Teal = "139, 213, 202";
          Sky = "145, 215, 227";
          Sapphire = "125, 196, 228";
          Blue = "138, 173, 244";
          Lavendar = "183, 189, 248";
          Text = "202, 211, 245";
          Subtext1 = "184, 192, 224";
          Subtext0 = "165, 173, 203";
          Overlay2 = "147, 154, 183";
          Overlay1 = "128, 135, 162";
          Overlay0 = "110, 115, 141";
          Surface2 = "91, 96, 120";
          Surface1 = "73, 77, 100";
          Surface0 = "54, 58, 79";
          Base = "36, 39, 58";
          Mantle = "30, 32, 48";
          Crust = "24, 25, 38";
        };
        hex = {
          Rosewater = "f4dbd6";
          Flamingo = "f0c6c6";
          Pink = "f5bde6";
          Mauve = "c6a0f6";
          Red = "ed8796";
          Maroon = "ee99a0";
          Peach = "f5a97f";
          Yellow = "eed49f";
          Green = "a6da95";
          Teal = "8bd5ca";
          Sky = "91d7e3";
          Sapphire = "7dc4e4";
          Blue = "8aadf4";
          Lavendar = "b7bdf8";
          Text = "cad3f5";
          Subtext1 = "b8c0e0";
          Subtext0 = "a5adcb";
          Overlay2 = "939ab7";
          Overlay1 = "8087a2";
          Overlay0 = "6e738d";
          Surface2 = "5b6078";
          Surface1 = "494d64";
          Surface0 = "363a4f";
          Base = "24273a";
          Mantle = "1e2030";
          Crust = "181926";
        };
      };
      Mocha = {
        hex = {
          Text = "cdd6f4";
          Mauve = "cba6f7";
          Sapphire = "74c7ec";
          Red = "f38ba8";
          Blue = "89b4fa";
          Lavender = "b4befe";
          Overlay2 = "9399b2";
          Overlay1 = "7f849c";
          Overlay0 = "6c7086";
          Surface2 = "585b70";
          Surface1 = "45475a";
          Surface0 = "313244";
          Base = "1e1e2e";
          Mantle = "181825";
          Crust = "11111b";
        };
      };
    };
  };
  gaps_out = 10;
  cursor = {
    name = "phinger-cursors-dark";
    package = pkgs.phinger-cursors;
    size = 32;
  };
in {
  home = {inherit username homeDirectory;};

  # imports = [
  #   inputs.hyprland.homeManagerModules.default
  # ];

  gtk = {
    enable = true;
    cursorTheme = cursor;
    iconTheme = {
      name = "Papirus";
      package = pkgs.papirus-icon-theme;
    };
  };

  home.pointerCursor = {
    inherit (cursor) name package size;
    gtk.enable = true;
  };

  wayland.windowManager.hyprland = {
    enable = true;
    settings = {
      general = {
        inherit gaps_out;
        gaps_in = 5;
        border_size = 1;
        "col.active_border" = "rgba(${colors.Catppuccin.Mocha.hex.Blue}FF)";
        layout = "dwindle";
        allow_tearing = true; # I have a gsync monitor
      };

      monitor = ", preferred, auto, 1";

      decoration = {
        rounding = 10;

        blur = {
          new_optimizations = true;
        };
      };

      dwindle = {
        no_gaps_when_only = false;
        preserve_split = true;
      };

      exec-once = [
        # "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1"
        # "sleep 3 && ${config.programs.waybar.package}/bin/waybar"
        "${config.services.hyprpaper.package}/bin/hyprpaper"
        "${pkgs.hyprland}/bin/hyprctl setcursor ${config.gtk.cursorTheme.name} ${toString config.gtk.cursorTheme.size}"
      ];

      env = [
        "LIBVA_DRIVER_NAME, nvidia"
        "XDG_SESSION_TYPE, wayland"
        "XDG_CURRENT_DESKTOP, Hyprland"
        "GBM_BACKEND, nvidia-drm"
        "__GLX_VENDOR_LIBRARY_NAME, nvidia"
        "WLR_NO_HARDWARE_CURSORS, 1"
      ];

      "$mod" = "SUPER";
      "$actionLayer" = "SUPER SHIFT";
      "$observerLayer" = "CTRL ALT";

      input = {
        # kb_options = "caps:swapescape";
        repeat_delay = 260;
        repeat_rate = 50;
        sensitivity = 0.3;
        accel_profile = "flat";
      };

      bind = [
        # "$actionLayer, q, exit"
        "$actionLayer, q, exec, ${config.programs.wlogout.package}/bin/wlogout"
        "$mod, q, killactive"
        "$mod, v, togglefloating"
        "$mod, f, fullscreen, 0" # true, app-aware fullscreen
        "$mod, m, fullscreen, 1" # maximize focus on workspace
        "$mod, s, togglesplit"

        # Copy a screenshot to the clipboard
        ", Print, exec, ${pkgs.grim}/bin/grim -g \"$(${pkgs.slurp}/bin/slurp -d)\" - | ${pkgs.wl-clipboard}/bin/wl-copy"

        # Save full screen
        "$mod, F2, exec, ${pkgs.grim}/bin/grim \"$(xdg-user-dir PICTURES)/Screenshots/$(date +'%s_grim.png')\""

        # Save a clipped screenshot
        "$mod, F3, exec, ${pkgs.grim}/bin/grim -g \"$(${pkgs.slurp}/bin/slurp -d)\" \"$(xdg-user-dir PICTURES)/Screenshots/$(date +'%s_grim.png')\""

        ", XF86AudioPlay, exec, ${pkgs.playerctl}/bin/playerctl play-pause"
        ", XF86AudioPrev, exec, ${pkgs.playerctl}/bin/playerctl previous"
        ", XF86AudioNext, exec, ${pkgs.playerctl}/bin/playerctl next"

        ", XF86AudioRaiseVolume, execr, ${pkgs.wireplumber}/bin/wpctl set-volume -l 1.0 @DEFAULT_AUDIO_SINK@ 5%+"
        ", XF86AudioLowerVolume, execr, ${pkgs.wireplumber}/bin/wpctl set-volume -l 1.0 @DEFAULT_AUDIO_SINK@ 5%-"
        ", XF86AudioMute, execr, ${pkgs.wireplumber}/bin/wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"

        "$mod, Space, exec, ${config.programs.fuzzel.package}/bin/fuzzel"
        "$mod, Return, exec, kitty"
        "$mod, e, exec, nautilus"

        "$mod, h, movefocus, l"
        "$mod, j, movefocus, d"
        "$mod, k, movefocus, u"
        "$mod, l, movefocus, r"

        "$actionLayer, h, movewindow, l"
        "$actionLayer, j, movewindow, d"
        "$actionLayer, k, movewindow, u"
        "$actionLayer, l, movewindow, r"

        "$mod, 1, workspace, 1"
        "$mod, 2, workspace, 2"
        "$mod, 3, workspace, 3"
        "$mod, 4, workspace, 4"
        "$mod, 5, workspace, 5"
        "$mod, 6, workspace, 6"
        "$mod, 7, workspace, 7"
        "$mod, 8, workspace, 8"
        "$mod, 9, workspace, 9"

        "$actionLayer, 1, movetoworkspace, 1"
        "$actionLayer, 2, movetoworkspace, 2"
        "$actionLayer, 3, movetoworkspace, 3"
        "$actionLayer, 4, movetoworkspace, 4"
        "$actionLayer, 5, movetoworkspace, 5"
        "$actionLayer, 6, movetoworkspace, 6"
        "$actionLayer, 7, movetoworkspace, 7"
        "$actionLayer, 8, movetoworkspace, 8"
        "$actionLayer, 9, movetoworkspace, 9"

        "$mod, Tab, workspace, e+1"
        "$mod, Escape, workspace, e-1"
        "$observerLayer, l, workspace, +1"
        "$observerLayer, h, workspace, -1"
        "$mod, mouse_down, workspace, e+1"
        "$mod, mouse_up, workspace, e-1"
      ];

      bindm = [
        "$mod, mouse:272, movewindow"
        "$mod, mouse:273, resizewindow"
      ];

      opengl = {
        nvidia_anti_flicker = true;
        force_introspection = 0;
      };

      xwayland.force_zero_scaling = true;

      misc = {
        vrr = 1;
        disable_autoreload = false;
      };
    };

    xwayland.enable = true;

    systemd = {
      enable = true;
      enableXdgAutostart = true;
      variables = ["--all"];
    };
  };

  dconf.settings = with lib.hm.gvariant; {
    "org/gnome/desktop/peripherals/keyboard" = {
      delay = mkUint32 250;
      repeat-interval = mkUint32 30;
    };

    "org/gnome/desktop/background" = rec {
      picture-uri = "file:///home/kas/Pictures/Wallpapers/lock-wallpaper-edit.png";
      picture-uri-dark = picture-uri;
    };

    "org/gnome/desktop/input-sources" = {
      sources = [(mkTuple ["xkb" "us"])];

      # uncomment this if you want to swap caps and escape in software
      # xkb-options = ["terminate:ctrl_alt_bksp" "caps:swapescape"];
    };

    "org/gnome/mutter" = {
      edge-tiling = true;
    };

    "org/gnome/nautilus/preferences" = {
      default-folder-viewer = "icon-view";
      migrated-gtk-settings = true;
      search-filter-time-type = "last_modified";
    };

    "org/gnome/shell/weather" = {
      automatic-location = true;

      # This doesn't work for some reason :(
      # locations = [(mkVariant [(mkUint32 2) (mkVariant ["Toronto" "CYTZ" true [(mkTuple [0.761545324469095 (-1.3857914260834978)])] [(mkTuple [0.7621271125219548 (-1.3860823201099277)])]])])];
    };

    "org/gnome/desktop/wm/keybindings" = {
      close = ["<Super>q"];
      switch-to-workspace-1 = ["<Super>1"];
      switch-to-workspace-2 = ["<Super>2"];
      switch-to-workspace-3 = ["<Super>3"];
      switch-to-workspace-4 = ["<Super>4"];
      switch-to-workspace-5 = ["<Super>5"];
      switch-to-workspace-6 = ["<Super>6"];
      switch-to-workspace-7 = ["<Super>7"];
      switch-to-workspace-8 = ["<Super>8"];
      switch-to-workspace-9 = ["<Super>9"];
      switch-to-workspace-left = ["<Shift><Super>comma"];
      switch-to-workspace-right = ["<Shift><Super>period"];
    };

    "org/gnome/tweaks" = {
      show-extensions-notice = false;
    };
  };

  home.sessionVariables.STEAM_EXTRA_COMPAT_TOOLS_PATH = "${homeDirectory}/.steam/root/compatabilitytools.d";

  programs.waybar = {
    enable = true;
    systemd = {
      enable = true;
      target = "hyprland-session.target";
    };
    settings = {
      mainBar = {
        margin-left = 10;
        margin-right = 10;
        margin-top = 5;
        margin-bottom = 0;
        reload_style_on_change = true;
        layer = "top";
        position = "top";
        height = 32;
        spacing = 15;
        # output = [
        #   "DP-2"
        # ];
        modules-left = ["custom/logo" "hyprland/workspaces" "hyprland/window"];
        modules-center = ["clock"];
        modules-right = ["cpu" "temperature" "memory" "network" "wireplumber" "clock#clock"];

        "custom/logo" = {
          format = "";
          on-click = "${config.programs.wlogout.package}/bin/wlogout";
        };

        temperature = {
          format = " {temperatureC}°C";

          # On Terra this is `k10temp` as indicated by the `name`.
          hwmon-path = "/sys/class/hwmon/hwmon2/temp1_input";
        };

        "hyprland/workspaces" = {
          on-click = "activate";
          format = "{icon}";
          # all-outputs = true;
          format-icons = {
            "1" = "";
            "2" = "";
            "3" = "";
            "4" = "";
            "5" = "";
          };
          persistent-workspaces = {
            "*" = 5;
          };
        };

        "cpu" = {
          # format = " {usage}%";
          # {max_frequency}Ghx or {avg_frequency}Ghz
          format = " {icon0} {icon1} {icon2} {icon3} {icon4} {icon5} {icon6} {icon7} {icon8} {icon9} {icon10} {icon11}";
          interval = 10;
          format-icons = ["▁" "▂" "▃" "▄" "▅" "▆" "▇" "█"];
        };

        "memory" = {
          format = " {percentage}%";
          interval = 30;
        };

        "network" = {
          format-wifi = " {essid} ({signalStrength}%)";
          format-ethernet = " {ipaddr}/{cidr}";
          format-disconnected = "";
        };

        wireplumber = {
          # format = " {icon}";
          # format-muted = " {icon}";
          format = "{icon} {volume}%";
          format-muted = " {volume}%";
          on-click = "${config.services.easyeffects.package}/bin/easyeffects";
          format-icons = {
            default = ["" "" ""];
            # default = ["▏" "▎" "▍" "▌" "▋" "▊" "▉" "█"];
            # default = ["▁" "▂" "▃" "▄" "▅" "▆" "▇" "█"];
          };
        };

        "clock" = {
          "format" = " {:%A, %B %d}";
        };

        "clock#clock" = {
          "format" = " {:%H:%M}";
        };

        "hyprland/window" = {
          format = " {title}";
          max-length = 50;
        };
      };
    };
    style = let
      sm = "0.125rem";
      smp = "0.25rem";
      md = "0.375rem";
      lg = "0.5rem";
      xl = "0.75rem";
      xl2 = "1rem";
      xl3 = "1.5rem";
    in
      with colors.Catppuccin.Macchiato.rgb; ''
        * {
          margin: 0;
          box-shadow: none;
          min-height: 0;
          font-family: "Noto Sans", "Font Awesome 6 Free", "Font Awesome 6 Brands";
          font-size: 16px;
        }

        label.module,
        #workspaces,
        #workspaces > button {
          border-radius: ${md};
        }

        label.module {
          padding: 0 ${xl};
          background-color: rgb(${Base});
        }

        window#waybar {
          background-color: rgb(${Crust});
          border-style: solid;
          border-radius: ${lg};
          border-width: 1px;
          border-color: rgb(${Surface2});
        }

        window#waybar > * {
          margin: 1px;
        }

        #custom-logo {
          background: transparent;
          color: rgb(${Blue});
          font-size: 22px;
        }

        #window {
          color: rgb(${Peach});
        }

        #workspaces {
          padding: ${sm};
          background-color: rgb(${Base});
        }

        #workspaces button {
          padding: ${md} ${xl};
          color: rgb(${Text});
        }

        #workspaces button.empty {
          color: rgb(${Surface1});
        }

        #workspaces button.active {
          color: rgb(${Blue});
        }

        /* make window module transparent when no windows present */
        window#waybar.empty #window {
          color: transparent;
          background-color: transparent;
        }

        #network {
          color: rgb(${Green});
        }

        #temperature {
          color: rgb(${Maroon});
        }

        #cpu {
          color: rgb(${Teal});
        }

        #clock {
          color: rgb(${Text});
        }

        #memory {
          color: rgb(${Peach});
        }

        #wireplumber {
          color: rgb(${Sky});
        }

        #wireplumber.muted {
          color: rgb(${Subtext1});
        }
      '';
  };

  programs.hyprlock = {
    enable = true;
    settings = {
      general = {
        hide_cursor = true;
        ignore_empty_input = true;
      };

      # background = [
      #   {
      #     path = "screenshot";
      #     blur_passes = 3;
      #     blur_size = 8;
      #   }
      # ];

      input-field = [
        {
          size = "200, 50";
          position = "0, -80";
          monitor = "";
          dots_center = true;
          fade_on_empty = false;
          font_color = "rgb(202, 211, 245)";
          inner_color = "rgb(91, 96, 120)";
          outer_color = "rgb(24, 25, 38)";
          outline_thickness = 3;
          shadow_passes = 2;
        }
      ];
    };
  };

  programs.fuzzel = {
    enable = true;
    settings = {
      main = {
        font = "Noto Sans:size=18";
        icon-theme = config.gtk.iconTheme.name;
      };
      colors = with colors.Catppuccin.Macchiato.hex; {
        background = "#${Crust}f9";
        selection = "#${Surface2}f9";
        text = "#${Text}ff";
        match = "#${Blue}ff";
        selection-match = "#${Blue}ff";
        selection-text = "#${Text}ff";
        border = "#${Blue}ff";
      };
    };
  };

  qt = {
    enable = false;
  };

  services.hyprpaper = {
    enable = true;
    settings = let
      wpd = "~/Pictures/Wallpapers";
    in {
      ipc = "on";
      splash = false;
      preload = ["${wpd}/nix-dark.png"];
      wallpaper = [",${wpd}/nix-dark.png"];
    };
  };

  programs.wlogout = let
    icons = "${config.programs.wlogout.package}/share/wlogout/icons";
  in {
    enable = true;
    style = with colors.Catppuccin.Macchiato.hex; ''
      * {
        background-image: none;
        box-shadow: none;
      }

      window {
        background-color: rgba(17, 17, 27, 0.9);
      }

      button {
        border-radius: 0;
        border-color: black;
        text-decoration-color: #FFFFFF;
        color: #FFFFFF;
        background-color: #${Crust};
        border-style: solid;
        border-width: 1px;
        background-repeat: no-repeat;
        background-position: center;
        background-size: 25%;
      }

      button:focus, button:active, button:hover {
        background-color: #${Base};
        outline-style: none;
      }

      #lock {
        background-image: image(url("${icons}/lock.png"));
      }

      #logout {
        background-image: image(url("${icons}/logout.png"));
      }

      #suspend {
        background-image: image(url("${icons}/suspend.png"));
      }

      #hibernate {
        background-image: image(url("${icons}/hibernate.png"));
      }

      #shutdown {
        background-image: image(url("${icons}/shutdown.png"));
      }

      #reboot {
        background-image: image(url("${icons}/reboot.png"));
      }
    '';
  };

  services.mako = with colors.Catppuccin.Macchiato.hex; {
    enable = true;
    textColor = "#${Text}";
    backgroundColor = "#${Crust}";
    borderColor = "#${Blue}";
    progressColor = "#${Base}";
    extraConfig = ''
      [urgency=high]
      border-color=#${Red}
    '';
    borderRadius = 10;
    borderSize = 1;
    height = 150;
    width = 450;
    defaultTimeout = 10000; # in milliseconds
    font = "Noto Sans 14";
    padding = "10";
    icons = true;
    layer = "top";
    maxVisible = 5; # set to -1 to always show all
  };

  services.easyeffects = {
    enable = true;
  };

  programs.zathura = {
    enable = true;
    mappings = {
      i = "recolor";
      u = "scroll half-up";
      d = "scroll half-down";
      D = "toggle_page_mode";
      p = "print";
    };
    options = {
      font = "Roboto Mono 11";
      recolor = "true";
      window-title-basename = "true";
      window-title-home-tilde = "true";
      statusbar-basename = "true";
      selection-clipboard = "clipboard";
      statusbar-fg = "#d8dee9";
      statusbar-bg = "#4c566a";
      statusbar-h-padding = "10";
      statusbar-v-padding = "10";
      notification-error-bg = "#bf616a";
      notification-error-fg = "#d8dee9";
      notification-warning-bg = "#bf616a";
      notification-warning-fg = "#d8dee9";
      index-bg = "#3b4252";
      index-fg = "#d8dee9";
      index-active-fg = "#3b4252";
      index-active-bg = "#88c0d0";
      highlight-color = "#4c566a";
      highlight-active-color = "#88c0d0";
      completion-bg = "#1d1f21";
      completion-fg = "#ffffff";
    };
  };

  programs.keychain.keys = ["git"];

  programs.nix-index = {
    enable = true;
    enableZshIntegration = true;
  };
}
