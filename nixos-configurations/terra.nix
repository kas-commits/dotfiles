{
  inputs,
  config,
  # lib,
  pkgs,
  ...
}: let
  username = "kas";
in {
  nix = {
    settings = {
      sandbox = "relaxed";
      extra-experimental-features = "nix-command flakes";
    };
    gc = {
      automatic = true;
    };
  };

  imports = [
    inputs.agenix.nixosModules.default
  ];

  system.stateVersion = "24.11";

  nixpkgs = {
    hostPlatform = "x86_64-linux";
    config = {
      allowUnfree = true;
    };
  };

  systemd = {
    # user.services.polkit-gnome-authentication-agent-1 = {
    #   description = "Start Gnome's Polkit Authentication Agent for Hyprland";
    #   wantedBy = ["hyprland-session.target"];
    #   wants = ["hyprland-session.target"];
    #   after = ["hyprland-session.target"];
    #   serviceConfig = {
    #     Type = "simple";
    #     ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
    #     Restart = "on-failure";
    #     RestartSec = 1;
    #     TimeoutStopSec = 10;
    #   };
    # };
  };

  hardware = {
    enableAllFirmware = true;
    enableRedistributableFirmware = true;

    cpu = {
      amd = {
        updateMicrocode = config.hardware.enableRedistributableFirmware;
      };
    };

    # Using Pipewire instead.
    pulseaudio.enable = false;

    nvidia = {
      package = config.boot.kernelPackages.nvidiaPackages.beta;
      # package = config.boot.kernelPackages.nvidiaPackages.mkDriver {
      #   version = "555.42.02";
      #   sha256_64bit = "sha256-k7cI3ZDlKp4mT46jMkLaIrc2YUx1lh1wj/J4SVSHWyk";
      #   sha256_aarch64 = lib.fakeSha256;
      #   openSha256 = lib.fakeSha256;
      #   settingsSha256 = "sha256-rtDxQjClJ+gyrCLvdZlT56YyHQ4sbaL+d5tL4L4VfkA=";
      #   persistencedSha256 = lib.fakeSha256;
      # };

      # Kernel Mode Setting (KMS) is a method for setting display resolution
      # and depth in the kernel space rather than user space. Typically this
      # isn't required but if you're experiencing certain issues try turning
      # this on to see if it helps.
      modesetting.enable = true;

      # Use the NVidia open source kernel module (not to be confused with the
      # independent third-party "nouveau" open source driver).
      # Support is limited to the Turing and later architectures. Full list of
      # supported GPUs is at:
      # https://github.com/NVIDIA/open-gpu-kernel-modules#compatible-gpus
      # Only available from driver 515.43.04+
      # Currently alpha-quality/buggy, so false is currently the
      # recommended setting
      open = false;

      # Enable the Nvidia settings menu,
      # accessible via `nvidia-settings`.
      nvidiaSettings = true;
    };

    opengl = {
      enable = true;

      # On 64-bit systems, whether to support Direct Rendering for 32-bit
      # applications (such as Wine). This is currently only supported for the
      # nvidia as well as Mesa.
      driSupport32Bit = true;
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/e13a00a8-0700-45fa-960b-159babe0eb3b";
      fsType = "ext4";
    };
    "/boot" = {
      device = "/dev/disk/by-uuid/C093-CA5D";
      fsType = "vfat";
      options = ["fmask=0022" "dmask=0022"];
    };
  };

  security = {
    # Pipewire (i.e. audio/video) setup
    rtkit.enable = true;

    pam.services.hyprlock = {};

    # Polkit is used for user processes to communicate with super-user
    # processes. A very common example is interacting with services spawned by
    # systemd
    polkit.enable = true;
  };

  networking = {
    # wireless.enable = false;
    networkmanager.enable = true;
    hostName = "terra";
    # defaultGateway  = { address = "192.168.1.1"; interface = "enp3s0"; };
    # nameservers = ["1.1.1.1" "8.8.8.8"];
  };

  services = {
    udev = {
      enable = true;
      extraRules = ''
        KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{serial}=="*vial:f64c2b3c*", MODE="0660", GROUP="${toString config.users.groups.users.gid}", TAG+="uaccess", TAG+="udev-acl"
      '';
    };

    gnome = {
      sushi.enable = true;
      gnome-settings-daemon.enable = true;
      gnome-keyring.enable = true;
      glib-networking.enable = true;
      games.enable = false;
    };

    displayManager = {
      autoLogin = {
        enable = false;
      };
      sddm = {
        enable = false;
        wayland.enable = true;
      };
    };

    desktopManager = {
      plasma6 = {
        enable = false;
      };
    };

    xserver = {
      enable = true;
      autorun = true;
      xkb.layout = "us";
      displayManager = {
        gdm = {
          enable = true;
          wayland = true;
        };
      };
      desktopManager = {
        gnome.enable = true;
      };
      videoDrivers = ["nvidia"];
    };

    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      # If you want to use JACK applications, uncomment this
      #jack.enable = true;
      wireplumber = {
        enable = true;
        configPackages = [
          (pkgs.writeTextDir "share/wireplumber/bluetooth.lua.d/51-bluez-config.lua" ''
            bluez_monitor.properties = {
            	["bluez5.enable-sbc-xq"] = true,
            	["bluez5.enable-msbc"] = true,
            	["bluez5.enable-hw-volume"] = true,
            	["bluez5.headset-roles"] = "[ hsp_hs hsp_ag hfp_hf hfp_ag ]"
            }
          '')
        ];
      };
    };

    flatpak.enable = true;
    mullvad-vpn = {
      enable = true;
      package = pkgs.mullvad-vpn;
    };
  };

  time = {
    timeZone = "America/Toronto";
  };

  i18n = {
    # Other potential options:
    # en_CA.UTF-8/UTF-8 for Canadian English
    # fr_CA.UTF-8/UTF-8 for Canadian French
    # fr_FR.UTF-8/UTF-8 for French (the country) French
    defaultLocale = "en_US.UTF-8";
  };

  swapDevices = [];

  boot = {
    # Default linux Kernel for specified nixos registry (currently unstable).
    # Note that these packages do include the nvidia drivers linked inside as
    # they are heavily dependant on the kernel version and setup. This is why
    # we have to use the `config` object to specify what nvidia package to use
    # in the hardware section.
    # kernelPackages = pkgs.linuxPackages_lqx;
    # kernelPackages = pkgs.linuxPackages_latest;
    kernelPackages = pkgs.linuxPackages_6_8;

    kernelParams = ["nvidia.NVreg_EnableGpuFirmware=0"];

    kernelModules = ["kvm-amd" "thermal_sys" "x86_pkg_temp_thermal"];
    # extraModulePackages = with config.boot.kernelPackages; [ ];

    initrd = {
      availableKernelModules = [
        "nvme"
        "xhci_pci"
        "ahci"
        "usb_storage"
        "usbhid"
        "sd_mod"
      ];
      kernelModules = [];
      # luks = {
      #   devices."luks-f763e22d-f808-463c-a5c8-07b7c8b54efc" = {
      #     device = "/dev/disk/by-uuid/f763e22d-f808-463c-a5c8-07b7c8b54efc";
      #   };
      # };
    };

    # Use a tmpfs for /tmp instead of storing on-disk.
    tmp = {
      useTmpfs = true;

      # This setting requires explination since it's not intuitive. RAM is
      # allocated by the kernel on an as-needed basis. When a tmpfs is used,
      # systemd and the kernel will attempt to keep only as many pages held as
      # it expects to require given the current usage. What this setting
      # actually does in practice is set a "configured size," which due to the
      # kernel being heavily optimized acts a lot more like a "maximum size"
      # than an actual pre-allocated chunk. The default, not just for NixOS but
      # for most systems is 50%, which is way too much these days. However
      # the number you see here is not going to be permanently taken, so set
      # this as "the largest size you're comfortable having /tmp be."
      tmpfsSize = "50%";
    };

    loader = {
      # The boot menu can still be accessed by pressing Space.
      timeout = 1;
      efi = {
        canTouchEfiVariables = true;
      };
      systemd-boot = {
        consoleMode = "max";
        enable = true;
        editor = false;
        configurationLimit = 6;
      };
    };
  };

  # xdg.portal.wlr.enable = true;

  environment = {
    shells = [pkgs.bashInteractive pkgs.zsh];

    sessionVariables = {
      # unfortunately, many electron versions are woefully incapable of working under wayland. Once they're ready for the switch, either get rid of these variables or swap their value.
      # NIXOS_OZONE_WL = "1";
      # MOZ_ENABLE_WAYLAND = "0";
    };

    # # This removes certain default packages from being installed.
    # gnome.excludePackages =
    #   (with pkgs; [
    #     gnome-photos
    #     gnome-tour
    #   ])
    #   ++ (with pkgs.gnome; [
    #     cheese # webcam tool
    #     gnome-music
    #     gnome-terminal
    #     gedit # text editor
    #     epiphany # web browser
    #     geary # email reader
    #     evince # document viewer
    #     gnome-characters
    #     totem # video player
    #     tali # poker game
    #     iagno # go game
    #     hitori # sudoku game
    #     atomix # puzzle game
    #   ]);

    # system-wide packages that should always be available.
    systemPackages = let
      custom_ungoogled-chromium = pkgs.ungoogled-chromium.override {
        enableWideVine = true;
      };
    in
      with pkgs; [
        # Tools
        git
        vim
        gnomeExtensions.clipboard-indicator
        gnomeExtensions.appindicator
        gnome.adwaita-icon-theme
        gnome.gnome-tweaks
        gnome.dconf-editor
        playerctl
        brave
        kitty
        papirus-icon-theme
        zathura
        libnotify
        xorg.xeyes
        openssl
        grim
        slurp
        wl-clipboard
        cava
        lm_sensors
        ffmpeg-full
        killall

        # This is an overlay tool when playing games. It has to be manually
        # activated per-game
        mangohud

        # This tool will impertively install one or multiple versions of proton,
        # possibly with patches or simply newer versions. To use, make sure the
        # STEAM_EXTRA_COMPAT_TOOLS_PATH env var is properly set beforehand.
        protonup

        # More gaming stuff
        lutris
        heroic
        bottles

        # Apps
        whatsapp-for-linux
        whatsapp-emoji-font
        spotify
        discord
        slack
        fragments
        bitwarden-desktop
        vial
        tagger
        g4music
        insomnia
        vlc
        mpv
        shotcut
        amberol
        impression
        switcheroo
        newsflash
        hieroglyphic
        mullvad-browser
        custom_ungoogled-chromium

        # Containers
        dive
        podman-tui
        podman-compose
      ];
  };

  fonts = {
    fontDir.enable = true;
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      liberation_ttf
      fira-code
      fira-code-symbols
      mplus-outline-fonts.githubRelease
      dina-font
      proggyfonts
      font-awesome
      (nerdfonts.override {
        fonts = [
          "RobotoMono"
          "JetBrainsMono"
          "SourceCodePro"
        ];
      })
    ];
  };

  users = {
    defaultUserShell = pkgs.zsh;
    mutableUsers = true;

    users = {
      root.password = null;

      "${username}" = {
        useDefaultShell = true;
        isNormalUser = true;
        createHome = true;
        home = "/home/${username}";
        extraGroups = ["wheel" "docker" "networkmanager" "input"];

        # This is just for creating an initial password to first login to the
        # user. Change this on first login using passwd.
        initialPassword = "password";
      };
    };
  };

  virtualisation = {
    containers = {
      enable = true;
    };

    docker = {
      enable = true;
    };

    podman = {
      enable = true;

      # Create a `docker` alias for podman, to use it as a drop-in replacement
      dockerCompat = false;

      enableNvidia = true;

      # Required for containers under podman-compose to be able to talk to each other.
      defaultNetwork.settings.dns_enabled = true;
    };
  };

  qt = {
    enable = false;
    platformTheme = "gnome";
    style = "adwaita-dark";
  };

  programs = {
    zsh.enable = true;

    firefox = {
      enable = true;
      package = pkgs.firefox-beta;
    };

    ssh = {
      startAgent = true;
      package = pkgs.openssh_hpn;
      enableAskPassword = true;
      # askPassword = pkgs.lib.mkForce "${pkgs.plasma5Packages.ksshaskpass.out}/bin/ksshaskpass";

      # set this if you want to force re-authentication after a certain period
      # of time.
      agentTimeout = null;
    };

    dconf.enable = true;

    hyprland = {
      enable = true;
      xwayland.enable = true;
    };

    steam = {
      enable = true;
      gamescopeSession.enable = true;
    };

    gamemode.enable = true;
  };
}
