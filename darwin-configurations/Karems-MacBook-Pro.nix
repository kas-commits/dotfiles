# you can also include the inputs and pkgs as inputs here
_: {
  users.users.root.home = "/var/root";
  nixpkgs.hostPlatform = "aarch64-darwin";
}
