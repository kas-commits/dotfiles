# you can also include the inputs here
{pkgs, ...}: {
  environment = {
    shells = [pkgs.zsh];
    systemPackages = with pkgs; [vim cacert];
  };

  programs = {zsh = {enable = true;};};

  homebrew = {
    enable = true;

    brews = [
      # "neovide"
    ];

    casks = [
      "kitty"
    ];

    global = {
      autoUpdate = true;
    };
  };

  # Auto upgrade nix package and the daemon service.
  services.nix-daemon.enable = true;
  # nix.package = pkgs.nix;
}
