{inputs, ...}: {
  imports = [
    inputs.nixvim.homeManagerModules.nixvim
    inputs.agenix.homeManagerModules.default
    ./programs
    ./shell
    ./xdg
  ];

  fonts.fontconfig.enable = true;

  home.stateVersion = "24.05";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
