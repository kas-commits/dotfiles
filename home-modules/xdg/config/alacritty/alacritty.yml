window:
  # opacity: 0.95
  opacity: 1

  decorations_theme_variant: Dark

  dimensions:
    columns: 80
    lines: 24

    # Window padding (changes require restart)
    #
    # Blank space added around the window in pixels. This padding is scaled
    # by DPI and the specified value is always added at both opposing sides.
  padding:
    x: 0
    y: 0

    # Spread additional padding evenly around the terminal content.
    # dynamic_padding: false

    # Window decorations
    #
    # Values for `decorations`:
    #     - full: Borders and title bar
    #     - none: Neither borders nor title bar
    #
    # Values for `decorations` (macOS only):
    #     - transparent: Title bar, transparent background and title bar buttons
    #     - buttonless: Ttle bar, transparent background, but no title bar buttons
  decorations: none

  # Startup Mode (changes require restart)
  #
  # Values for `startup_mode`:
  #   - Windowed
  #   - Maximized
  #   - Fullscreen
  #
  # Values for `startup_mode` (macOS only):
  #   - SimpleFullscreen
  startup_mode: Maximized

  # Window title
  #title: Alacritty

  # Window class (Linux/BSD only):
  # class:
  # Application instance name
  # instance: Alacritty
  # General application class
  # general: Alacritty

  dynamic_title: true

scrolling:
  # Maximum number of lines in the scrollback buffer.
  # Specifying '0' will disable scrolling.
  history: 10000

  # Number of lines the viewport will move for every line scrolled when
  # scrollback is enabled (history > 0).
  #multiplier: 3

font:
  normal:
    # family: JetBrainsMono Nerd Font Mono
    # family: Hack Nerd Font Mono
    # family: SauceCodePro Nerd Font Mono
    # family: FiraCode Nerd Font Mono
    family: RobotoMono Nerd Font
    # family: DejaVuSansMono Nerd Font Mono
    # family: IosevkaTerm Nerd Font Mono

    style: Regular
    # style: Light
  bold:
    style: Medium
    # style: Bold
    # style: Regular
  italic:
    # style: Oblique
    style: Italic
  bold_italic:
    style: Medium Italic
    # style: Bold Oblique
    # style: Bold Italic
  size: 13.5
  # size: 12.5

draw_bold_text_with_bright_colors: true

# Any items in the `env` entry below will be added as
# environment variables. Some entries may override variables
# set by alacritty itself.
# env:
  # TERM variable
  #
  # This value is used to set the `$TERM` environment variable for
  # each instance of Alacritty. If it is not present, alacritty will
  # check the local terminfo database and use `alacritty` if it is
  # available, otherwise `xterm-256color` is used.
  # TERM: tmux-256color

# colors:
#   # Default colors
#   primary:
#     background: '0x1e222a'
#     foreground: '0x979eab'
#   # Colors the cursor will use if `custom_cursor_colors` is true
#   cursor:
#     text: '0x979eab'
#     cursor: '0x1a1b26'
#   # Normal colors
#   normal:
#     black:   '0x353b45'
#     red:     '0xe06c75'
#     green:   '0x98c379'
#     yellow:  '0xe5c07b'
#     blue:    '0x61afef'
#     magenta: '0x9a7ecc'
#     cyan:    '0x56b6c2'
#     white:   '0xabb2bf'
#   # Bright colors
#   bright:
#     black:   '0x393e48'
#     red:     '0xe06c75'
#     green:   '0x98c379'
#     yellow:  '0xe5c07b'
#     blue:    '0x61afef'
#     magenta: '0x9a7ecc'
#     cyan:    '0x56b6c2'
#     white:   '0xabb2bf'

# colors:
#   primary:
#     background: "#282a36"
#     foreground: "#eff0eb"
#   cursor:
#     cursor: "#97979b"
#   selection:
#     text: "#282a36"
#     background: "#feffff"
#   normal:
#     black: "#282a36"
#     red: "#ff5c57"
#     green: "#5af78e"
#     yellow: "#f3f99d"
#     blue: "#57c7ff"
#     magenta: "#ff6ac1"
#     cyan: "#9aedfe"
#     white: "#f1f1f0"
#   bright:
#     black: "#686868"
#     red: "#ff5c57"
#     green: "#5af78e"
#     yellow: "#f3f99d"
#     blue: "#57c7ff"
#     magenta: "#ff6ac1"
#     cyan: "#9aedfe"
#     white: "#eff0eb"

colors:
    # Default colors
    primary:
        background: "#24273A" # base
        foreground: "#CAD3F5" # text
        # Bright and dim foreground colors
        dim_foreground: "#CAD3F5" # text
        bright_foreground: "#CAD3F5" # text

    # Cursor colors
    cursor:
        text: "#24273A" # base
        cursor: "#F4DBD6" # rosewater
    vi_mode_cursor:
        text: "#24273A" # base
        cursor: "#B7BDF8" # lavender

    # Search colors
    search:
        matches:
            foreground: "#24273A" # base
            background: "#A5ADCB" # subtext0
        focused_match:
            foreground: "#24273A" # base
            background: "#A6DA95" # green
        footer_bar:
            foreground: "#24273A" # base
            background: "#A5ADCB" # subtext0

    # Keyboard regex hints
    hints:
        start:
            foreground: "#24273A" # base
            background: "#EED49F" # yellow
        end:
            foreground: "#24273A" # base
            background: "#A5ADCB" # subtext0

    # Selection colors
    selection:
        text: "#24273A" # base
        background: "#F4DBD6" # rosewater

    # Normal colors
    normal:
        black: "#494D64" # surface1
        red: "#ED8796" # red
        green: "#A6DA95" # green
        yellow: "#EED49F" # yellow
        blue: "#8AADF4" # blue
        magenta: "#F5BDE6" # pink
        cyan: "#8BD5CA" # teal
        white: "#B8C0E0" # subtext1

    # Bright colors
    bright:
        black: "#5B6078" # surface2
        red: "#ED8796" # red
        green: "#A6DA95" # green
        yellow: "#EED49F" # yellow
        blue: "#8AADF4" # blue
        magenta: "#F5BDE6" # pink
        cyan: "#8BD5CA" # teal
        white: "#A5ADCB" # subtext0

    # Dim colors
    dim:
        black: "#494D64" # surface1
        red: "#ED8796" # red
        green: "#A6DA95" # green
        yellow: "#EED49F" # yellow
        blue: "#8AADF4" # blue
        magenta: "#F5BDE6" # pink
        cyan: "#8BD5CA" # teal
        white: "#B8C0E0" # subtext1

    indexed_colors:
        - { index: 16, color: "#F5A97F" }
        - { index: 17, color: "#F4DBD6" }


# # Kanagawa Alacritty Colors
# colors:
#   primary:
#     background: "0x1f1f28"
#     foreground: "0xdcd7ba"
#   normal:
#     black: "0x090618"
#     red: "0xc34043"
#     green: "0x76946a"
#     yellow: "0xc0a36e"
#     blue: "0x7e9cd8"
#     magenta: "0x957fb8"
#     cyan: "0x6a9589"
#     white: "0xc8c093"
#   bright:
#     black: "0x727169"
#     red: "0xe82424"
#     green: "0x98bb6c"
#     yellow: "0xe6c384"
#     blue: "0x7fb4ca"
#     magenta: "0x938aa9"
#     cyan: "0x7aa89f"
#     white: "0xdcd7ba"
#   selection:
#     background: "0x2d4f67"
#     foreground: "0xc8c093"
#   indexed_colors:
#     - { index: 16, color: "0xffa066" }
#     - { index: 17, color: "0xff5d62" }

# # onedark Alacritty Colors
# colors:
#   # Default colors
#   primary:
#     # background: "#282c34"
#     background: "#1f2329"
#     foreground: "#abb2bf"
#     # Normal colors
#   normal:
#     black: "#20232A"
#     red: "#e86671"
#     green: "#98c379"
#     yellow: "#e0af68"
#     blue: "#61afef"
#     magenta: "#c678dd"
#     cyan: "#56b6c2"
#     white: "#798294"
#     # Bright colors
#   bright:
#     black: "#21252b"
#     red: "#e86671"
#     green: "#98c379"
#     yellow: "#e0af68"
#     blue: "#61afef"
#     magenta: "#c678dd"
#     cyan: "#56b6c2"
#     white: "#abb2bf"
#   indexed_colors:
#     - { index: 16, color: "0xd19a66" }
#     - { index: 17, color: "0xf65866" }

# # Nord Theme
# colors:
#   primary:
#     background: '#2E3440'
#     foreground: '#D8DEE9'
#   cursor:
#     text: '#2E3440'
#     cursor: '#D8DEE9'
#   normal:
#     black: '#3B4252'
#     red: '#BF616A'
#     green: '#A3BE8C'
#     yellow: '#EBCB8B'
#     blue: '#5e81ac'
#     magenta: '#B48EAD'
#     cyan: '#88C0D0'
#     white: '#E5E9F0'
#   bright:
#     black: '#4C566A'
#     red: '#BF616A'
#     green: '#A3BE8C'
#     yellow: '#EBCB8B'
#     blue: '#5e81ac'
#     magenta: '#B48EAD'
#     cyan: '#88C0D0'
#     white: '#ECEFF4'

# # Gruvbox
# colors:
#   primary:
#     foreground: '#ebdbb2'
#     background: '#282828'
#   cursor:
#     cursor:     '#c5c8c6'
#   normal:
#     black:      '#333333'
#     red:        '#cc241d'
#     green:      '#98971a'
#     yellow:     '#d79921'
#     blue:       '#458588'
#     magenta:    '#b16286'
#     cyan:       '#689d6a'
#     white:      '#a89984'
#   bright:
#     black:      '#928374'
#     red:        '#fb4934'
#     green:      '#b8bb26'
#     yellow:     '#fabd2f'
#     blue:       '#83a598'
#     magenta:    '#d3869b'
#     cyan:       '#8ec07c'
#     white:      '#ebdbb2'

# # Solarized
# colors:
#   primary:
#     background: '#002b36'
#     foreground: '#93a1a1'
#   normal:
#     black:   '#002b36'
#     red:     '#dc322f'
#     green:   '#859900'
#     yellow:  '#b58900'
#     blue:    '#268bd2'
#     magenta: '#6c71c4'
#     cyan:    '#2aa198'
#     white:   '#93a1a1'
#   bright:
#     black:   '#657b83'
#     red:     '#dc322f'
#     green:   '#859900'
#     yellow:  '#b58900'
#     blue:    '#268bd2'
#     magenta: '#6c71c4'
#     cyan:    '#2aa198'
#     white:   '#fdf6e3'

# # Google Dark Scheme:
# colors:
#   primary:
#     background: '#1d1f21'
#     # background: '#000000'
#     # background: '#1a1a1a'
#     foreground: '#c5c8c6'
#   cursor:
#     text:       '#44475a'
#     cursor:     '#f8f8f2'
#   normal:
#     black:      '#1d1f21'
#     red:        '#cc342b'
#     green:      '#198844'
#     yellow:     '#fba922'
#     blue:       '#3971ed'
#     magenta:    '#a36ac7'
#     cyan:       '#00ccff'
#     white:      '#c5c8c6'
#   bright:
#     black:      '#333333'
#     red:        '#cc342b'
#     green:      '#198844'
#     yellow:     '#fba922'
#     blue:       '#3971ed'
#     magenta:    '#a36ac7'
#     cyan:       '#00ccff'
#     white:      '#ffffff'

# Blossom Scheme:
# colors:
#   primary:
#     background: '#22272C'
#     foreground: '#F2F2F4'
#   cursor:
#     text:       '#44475a'
#     cursor:     '#f8f8f2'
#   normal:
#     black:      '#445158'
#     red:        '#FF6565'
#     green:      '#2CC46E'
#     yellow:     '#ECC46F'
#     blue:       '#6F97EC'
#     magenta:    '#FA5477'
#     cyan:       '#64B9F2'
#     white:      '#D2D2D4'
#   bright:
#     black:      '#5E6B72'
#     red:        '#FF7272'
#     green:      '#46DE88'
#     yellow:     '#FFDE89'
#     blue:       '#89B1FF'
#     magenta:    '#FF6E91'
#     cyan:       '#71C6FF'
#     white:      '#F1F1F3'

# # Monokai Night
# colors:
#   primary:
#     background: '#1f1f1f'
#     foreground: '#f8f8f8'
#   cursor:
#     text:       '#44475a'
#     cursor:     '#f8f8f2'
#   normal:
#     black:      '#1f1f1f'
#     red:        '#f92672'
#     green:      '#a6e22e'
#     yellow:     '#e6db74'
#     blue:       '#6699df'
#     magenta:    '#ae81ff'
#     cyan:       '#e69f66'
#     white:      '#f8f8f2'
#   bright:
#     black:      '#75715e'
#     red:        '#f92672'
#     green:      '#a6e22e'
#     yellow:     '#e6db74'
#     blue:       '#66d9ef'
#     magenta:    '#ae81ff'
#     cyan:       '#e69f66'
#     white:      '#f8f8f2'

# background_opacity: 0.96

selection:
  #semantic_escape_chars: ",│`|:\"' ()[]{}<>\t"

  # When set to `true`, selected text will be copied to the primary clipboard.
  save_to_clipboard: true


cursor:
  # Values for `style`:
  #   - ▇ Block
  #   - _ Underline
  #   - | Beam
  style:
    shape: Underline
    blinking: Always
  unfocused_hollow: true
  # Cursor blinking state
  #
  # Values for `blinking`:
  #   - Never: Prevent the cursor from ever blinking
  #   - Off: Disable blinking by default
  #   - On: Enable blinking by default
  #   - Always: Force the cursor to always blink
  # blinking: Always
  # Vi mode cursor style
  #
  # If the vi mode cursor style is `None` or not specified, it will fall back to
  # the style of the active value of the normal cursor.
  #
  # See `cursor.style` for available options.
  vi_mode_style: Block
  # Cursor blinking interval in milliseconds.
  blink_interval: 350
  # Thickness of the cursor relative to the cell width as floating point number
  # from `0.0` to `1.0`.
  # thickness: 0.15

# Live config reload (changes require restart)
live_config_reload: true

# shell:
#   program: fish

# To trigger mouse bindings when an application running within Alacritty captures the mouse, the
# `Shift` modifier is automatically added as a requirement.
#
# Each mouse binding will specify a:
#   - Middle
#   - Left
#   - Right
#   - Numeric identifier such as `5`
mouse_bindings:
  - { mouse: Middle, action: PasteSelection }
  - { mouse: Right, action: PasteSelection }

  # Key bindings
  #
  # Each key binding will specify a:
  #
  # - `key`: Identifier of the key pressed
  #
  #    - A-Z
  #    - F1-F24
  #    - Key0-Key9
  #
  #    A full list with available key codes can be found here:
  #    https://docs.rs/glutin/*/glutin/event/enum.VirtualKeyCode.html#variants
  #
  #    Instead of using the name of the keys, the `key` field also supports using
  #    the scancode of the desired key. Scancodes have to be specified as a
  #    decimal number. This command will allow you to display the hex scancodes
  #    for certain keys:
  #
  #       `showkey --scancodes`.
  #
  # Then exactly one of:
  #
  # - `chars`: Send a byte sequence to the running application
  #
  #    The `chars` field writes the specified string to the terminal. This makes
  #    it possible to pass escape sequences. To find escape codes for bindings
  #    like `PageUp` (`"\x1b[5~"`), you can run the comman `showkey -a` outside
  #    of tmux. Note that applications use terminfo to map escape sequences back
  #    to keys. It is therefore required to update the terminfo when changing an
  #    escape sequence.
  #
  # - `action`: Execute a predefined action
  #
  #   - Copy
  #   - Paste
  #   - PasteSelection
  #   - IncreaseFontSize
  #   - DecreaseFontSize
  #   - ResetFontSize
  #   - ScrollPageUp
  #   - ScrollPageDown
  #   - ScrollLineUp
  #   - ScrollLineDown
  #   - ScrollToTop
  #   - ScrollToBottom
  #   - ClearHistory
  #   - Hide
  #   - Minimize
  #   - Quit
  #   - ToggleFullscreen
  #   - SpawnNewInstance
  #   - ClearLogNotice
  #   - ReceiveChar
  #   - None
  #
  # - `command`: Fork and execute a specified command plus arguments
  #
  #    The `command` field must be a map containing a `program` string and an
  #    `args` array of command line parameter strings. For example:
  #       `{ program: "alacritty", args: ["-e", "vttest"] }`
  #
  # And optionally:
  #
  # - `mods`: Key modifiers to filter binding actions
  #
  #    - Command
  #    - Control
  #    - Option
  #    - Super
  #    - Shift
  #    - Alt
  #
  #    Multiple `mods` can be combined using `|` like this:
  #       `mods: Control|Shift`.
  #    Whitespace and capitalization are relevant and must match the example.
  #
  # - `mode`: Indicate a binding for only specific terminal reported modes
  #
  #    This is mainly used to send applications the correct escape sequences
  #    when in different modes.
  #
  #    - AppCursor
  #    - AppKeypad
  #    - Alt
  #
  #    A `~` operator can be used before a mode to apply the binding whenever
  #    the mode is *not* active, e.g. `~Alt`.
  #
  # Bindings are always filled by default, but will be replaced when a new
  # binding with the same triggers is defined. To unset a default binding, it can
  # be mapped to the `ReceiveChar` action. Alternatively, you can use `None` for
  # a no-op if you do not wish to receive input characters for that binding.
key_bindings:
  - { key: V, mods: Control|Shift, action: Paste }
  - { key: M, mods: Control|Shift, action: ToggleFullscreen }
  - { key: N, mods: Control|Shift, action: SpawnNewInstance }
  - { key: C, mods: Control|Shift, action: Copy }
  - { key: Insert, mods: Shift, action: PasteSelection }
  - { key: Key0, mods: Control|Shift, action: ResetFontSize }
  - { key: Equals, mods: Control|Shift, action: IncreaseFontSize }
  - { key: Minus, mods: Control|Shift, action: DecreaseFontSize }
  - { key: Paste, action: Paste }
  - { key: Copy, action: Copy }
  - { key: J,        mods: Alt,     chars: "\x1bj"                       }
  - { key: K,        mods: Alt,     chars: "\x1bk"                       }
  - { key: B,        mods: Alt,     chars: "\x1bb"                       }
  - { key: F,        mods: Alt,     chars: "\x1bf"                       }
  - { key: H,        mods: Alt,     chars: "\x1bh"                       }
  - { key: L,        mods: Alt,     chars: "\x1bl"                       }
  - { key: Q,        mods: Alt,     chars: "\x1bq"                       }
  - { key: S,        mods: Alt,     chars: "\x1bs"                       }
  - { key: W,        mods: Alt,     chars: "\x1bw"                       }
