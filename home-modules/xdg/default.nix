_: {
  xdg = {
    enable = true;
    # cacheHome = "~/.cache";
    # configHome = "~/.config";
    # dataHome = "~/.local/share";
    # stateHome = "~/.local/state";

    configFile = {
      lf = {
        enable = true;
        recursive = true;
        source = ./config/lf;
        target = "lf";
      };

      # nvim = {
      #   enable = true;
      #   recursive = true;
      #   source = ./config/nvim;
      #   target = "nvim";
      # };

      kak = {
        enable = true;
        recursive = true;
        source = ./config/kak;
        target = "kak";
      };

      kak-lsp = {
        enable = true;
        recursive = true;
        source = ./config/kak-lsp;
        target = "kak-lsp";
      };

      nixpkgs = {
        enable = true;
        recursive = false;
        source = ./config/nixpkgs;
        target = "nixpkgs";
      };

      alacritty = {
        enable = true;
        recursive = true;
        source = ./config/alacritty;
        target = "alacritty";
      };

      kitty = {
        enable = true;
        recursive = true;
        source = ./config/kitty;
        target = "kitty";
      };
    };
  };
}
