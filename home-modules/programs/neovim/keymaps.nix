_: let
  baseOpts = {
    noremap = true;
    silent = true;
  };
  mkMap = mode: key: action: {
    inherit key action mode;
    options = baseOpts;
  };
  nMap = key: action: mkMap "n" key action;
  iMap = key: action: mkMap "i" key action;
  vMap = key: action: mkMap "v" key action;
  tMap = key: action: mkMap "t" key action;
in {
  programs.nixvim = {
    keymaps = [
      (nMap "Q" "<nop>")
      (nMap "Y" "y$")
      (nMap "<ESC>" "<cmd>noh<CR>")
      (nMap "n" "nzzzv")
      (nMap "N" "Nzzzv")
      (nMap "gl" "$")
      (nMap "ge" "G")
      (nMap "gh" "0")
      (nMap "gn" "<cmd>bn<CR>")
      (nMap "gp" "<cmd>bp<CR>")
      (nMap "U" "<C-r>")
      (nMap "J" "mzJ`z")
      (nMap "x" "\"_x")
      (vMap "x" "\"_x")
      (vMap "J" ":m \'>+1<CR>gv=gv")
      (vMap "K" ":m \'<-2<CR>gv=gv")
      (iMap "," ",<c-g>u")
      (iMap "." ".<c-g>u")
      (iMap "!" "!<c-g>u")
      (iMap "?" "?<c-g>u")
      (nMap "<A-q>" "<cmd>qa<CR>")
      (nMap "<A-s>" "<cmd>update<CR>")
      (nMap "<A-w>" "<cmd>Bdelete<CR>")
      (nMap "<A-h>" "<cmd>vertical resize -2<CR>")
      (nMap "<A-l>" "<cmd>vertical resize +2<CR>")
      (nMap "<A-j>" "<cmd>resize -2<CR>")
      (nMap "<A-k>" "<cmd>resize +2<CR>")
      (nMap "<leader>q" "<cmd>bd<CR>")
      (nMap "<leader>n" "<cmd>Neotree float reveal reveal_force_cwd<CR>")
      (nMap "<leader>o" "<cmd>setlocal spell!<CR>")
      (nMap "<leader>y" "\"+y")
      (nMap "<leader>Y" "\"+y$")
      (nMap "<leader>p" "\"+p")
      (nMap "<leader>P" "\"+P")
      (tMap "<ESC>" "<C-\\><C-n>")
      (tMap "<A-ESC>" "<ESC>")
      (tMap "<A-q>" "<C-\\><C-n><cmd>bd!<CR>")
    ];
  };
}
