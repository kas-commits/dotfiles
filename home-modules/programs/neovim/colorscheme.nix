_: {
  programs.nixvim = {
    colorschemes = {
      kanagawa = {
        enable = true;
        settings = {
          theme = "wave"; # Chose between wave, dragon or lotus
          commentStyle = {
            italic = true;
          };
        };
      };
      catppuccin = {
        enable = false;
        settings = {
          flavour = "mocha";
          show_end_of_buffer = false;
          dim_inactive.enabled = false;
          term_colors = true;
          styles = {
            comments = ["italic"];
            conditionals = ["italic"];
            # loops = [];
            # functions = [];
            # keywords = [];
            # strings = [];
            # variables = [];
            # numbers = [];
            # booleans = [];
            # properties = [];
            # types = [];
            # operators = [];
          };
        };
      };
    };
  };
}
