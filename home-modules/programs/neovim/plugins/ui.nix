_: {
  programs.nixvim.plugins = {
    bufferline = {
      enable = false;
      mode = "buffers";
      numbers = "none";
      showBufferCloseIcons = false;
      themable = false;
      showCloseIcon = false;
      showTabIndicators = false;
      alwaysShowBufferline = true;
      separatorStyle = "slant";
      indicator = {
        icon = ""; # default is "▎"
        style = "icon"; # could also be underline or none
      };
      diagnostics = "nvim_lsp";
      hover.enabled = false;
    };
    lualine = {
      enable = true;
      iconsEnabled = true;
    };
    indent-blankline = {
      enable = true;
      settings = {
        scope = {
          enabled = false;
          show_start = false;
          show_end = false;
        };
      };
    };
  };
}
