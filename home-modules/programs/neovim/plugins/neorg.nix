_: {
  programs.nixvim.plugins.neorg = {
    enable = true;
    lazyLoading = true;
    modules = {
      "core.defaults" = {__empty = null;};
      "core.concealer" = {config = {icon_preset = "basic";};};
      "core.completion" = {config = {engine = "nvim-cmp";};};
      "core.summary" = {__empty = null;};
      "core.dirman" = {
        config = {
          workspaces = {
            work = "~/notes/work";
            home = "~/notes/home";
          };
          default_workspace = "home";
        };
      };
    };
  };
}
