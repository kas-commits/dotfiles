_: {
  imports = [
    ./treesitter.nix
    ./lsp.nix
    ./completion.nix
    ./telescope.nix
    # ./neorg.nix
    ./ui.nix
    ./typing_experience.nix
    ./misc.nix
  ];
}
