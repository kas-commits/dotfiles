_: {
  programs.nixvim.plugins = {
    cmp-git.enable = true;
    cmp-nvim-lsp.enable = true;
    cmp = {
      enable = true;
      cmdline = {
        "/" = {
          mapping = {
            __raw = "cmp.mapping.preset.cmdline()";
          };
          sources = [
            {
              name = "buffer";
            }
          ];
        };
        ":" = {
          mapping = {
            __raw = "cmp.mapping.preset.cmdline()";
          };
          sources = [
            {
              name = "path";
            }
            {
              name = "cmdline";
              option = {
                ignore_cmds = [
                  "Man"
                  "!"
                ];
              };
            }
          ];
        };
      };
      settings = {
        sources = [
          {
            name = "nvim_lsp";
          }
        ];
        mapping = {
          "<c-d>" = "cmp.mapping.scroll_docs(4)";
          "<c-u>" = "cmp.mapping.scroll_docs(-4)";
          "<c-p>" = "cmp.mapping.select_prev_item()";
          "<c-n>" = "cmp.mapping.select_next_item()";
          "<c-x>" = "cmp.mapping.complete()";
          "<c-e>" = "cmp.mapping.close()";
          "<CR>" = "cmp.mapping.confirm({ select = true })";
        };
      };
    };
  };
}
