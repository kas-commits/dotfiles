_: {
  programs.nixvim.plugins = {
    comment = {
      enable = true;
      settings = {
        sticky = true;
      };
    };
    nvim-autopairs = {
      enable = true;
    };
  };
}
