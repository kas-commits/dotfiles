_: {
  programs.nixvim.plugins = {
    lspkind = {
      enable = true;
    };

    lsp = {
      enable = true;
      keymaps = {
        silent = true;
        diagnostic = {
          "]d" = "goto_next";
          "[d" = "goto_prev";
          "<leader>k" = "open_float";
        };
        lspBuf = {
          # "gd" = "definition";
          "gt" = "type_definition";
          "gi" = "implementation";
          "gD" = "declaration";
          "K" = "hover";
          "<leader>r" = "rename";
          "<leader>a" = "code_action";
        };
      };
      servers = {
        astro = {
          enable = true;
          # package = null;
        };
        tailwindcss = {
          enable = true;
          # package = null;
        };
        marksman = {
          enable = true;
          # package = null;
        };
        terraformls = {
          enable = true;
          # package = null;
        };
        nil_ls = {
          enable = true;
          # package = null;
        };
        pyright = {
          enable = true;
          package = null;
        };
        ruff-lsp = {
          enable = true;
          package = null;
        };
        tsserver = {
          enable = true;
        };
      };
    };

    lsp-format = {
      enable = true;
      lspServersToEnable = [
        "ruff-lsp"
        "terraformls"
      ];
    };
  };
}
