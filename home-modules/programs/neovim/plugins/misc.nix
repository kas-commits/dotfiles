{pkgs, ...}: {
  programs.nixvim = {
    plugins = {
      neo-tree = {
        enable = true;
        closeIfLastWindow = true;
        enableRefreshOnWrite = true;
        hideRootNode = true;
        sourceSelector = {
          highlightBackground = "";
        };
      };

      gitlinker = {
        enable = true;
      };

      neogit = {
        enable = true;
      };
    };

    extraPlugins = with pkgs.vimPlugins; [
      bufdelete-nvim
    ];
  };
}
