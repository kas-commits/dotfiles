_: {
  programs.nixvim.plugins.telescope = {
    enable = true;
    extensions = {
      file-browser = {
        enable = true;
        settings = {
          theme = "ivy";
        };
      };
      fzf-native = {
        enable = true;
        settings = {
          fuzzy = true;
          override_generic_sorter = true;
          override_file_sorter = true;
          case_mode = "smart_case";
        };
      };
    };
    keymaps = {
      "<leader>f" = "find_files";
      "gf" = "git_files";
      "gd" = "lsp_definitions";
      "gr" = "lsp_references";
      "<leader>/" = "live_grep";
      "<leader>s" = "spell_suggest";
      "<leader>b" = "buffers";
      "<leader>x" = "diagnostics";
    };
  };
}
