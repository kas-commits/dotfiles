_: {
  imports = [
    ./opts.nix
    ./colorscheme.nix
    ./keymaps.nix
    ./plugins
  ];

  programs.nixvim = {
    enable = true;
  };
}
