_: let
  indent = 2;
  colend = 79;
in {
  programs.nixvim = {
    globals = {
      mapleader = " ";
      maplocalleader = ",";
      neovide_input_use_logo = true;
      neovide_input_macos_alt_is_meta = true;
    };

    opts = {
      ch = 0;
      spell = false;
      colorcolumn = "80";
      hidden = true;
      showmode = false;
      laststatus = 3;
      autochdir = false;
      termguicolors = true;
      mouse = "a";
      splitbelow = true;
      splitright = true;
      scrolloff = 4;
      syntax = "off";

      conceallevel = 3;

      # # This option makes concealed things stay concealed even when you hover
      # # over them
      # concealcursor = "n";

      updatetime = 50;
      ignorecase = true;
      smartcase = true;
      list = true;
      listchars = "tab:> ,trail:-,nbsp:+,eol:⏎";
      title = true;
      autowrite = true;
      swapfile = false;
      backup = false;
      guicursor = "n-v-i-c:block,ci-ve:ver25,r-cr:hor20,o:hor50,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor,sm:block-blinkwait175-blinkoff150-blinkon175";
      guifont = "JetBrainsMono Nerd Font Mono:h14";
      textwidth = colend;
      wrap = true;
      cursorline = true;
      shiftwidth = indent;
      tabstop = indent;
      softtabstop = indent;
      expandtab = true;
      undofile = true;
    };
  };
}
