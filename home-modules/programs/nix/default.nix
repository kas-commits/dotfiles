{pkgs, ...}: {
  nix = {
    package = pkgs.nix;
    gc = {
      automatic = true;
      frequency = "weekly";
      options = "--delete-old";
    };
    settings = {
      experimental-features = ["nix-command" "flakes"];
      allow-dirty = true;
    };
  };
  nixpkgs = {
    config = import ../../xdg/config/nixpkgs/config.nix;
  };
}
