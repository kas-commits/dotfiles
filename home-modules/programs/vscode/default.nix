{pkgs, ...}: {
  programs.vscode = {
    enable = true;
    enableExtensionUpdateCheck = false;
    enableUpdateCheck = false;
    mutableExtensionsDir = true;
    package = pkgs.vscodium;
    # package = pkgs.vscode;

    extensions = with pkgs.vscode-extensions; [
      vscodevim.vim
      ms-python.python
      catppuccin.catppuccin-vsc
      catppuccin.catppuccin-vsc-icons
      astro-build.astro-vscode
      # github.copilot
      # github.copilot-chat
      ms-ceintl.vscode-language-pack-fr
      bradlc.vscode-tailwindcss
      ms-dotnettools.csharp
      ms-dotnettools.csdevkit
    ];

    userSettings = {
      "workbench.colorTheme" = "Catppuccin Macchiato";
      "workbench.sideBar.location" = "right";
      "workbench.statusBar.visible" = false;
      "workbench.tips.enabled" = false;

      # Apparently, Codium doesn't believe this is a recognized setting.
      # "workbench.activityBar.visible" = false;

      "breadcrumbs.enabled" = false;

      "editor.inlineSuggest.enabled" = true;
      "editor.minimap.enabled" = false;
      "editor.lineNumbers" = "off";
      "editor.rulers" = [80];
      "editor.glyphMargin" = false;
      "editor.folding" = false;
      "editor.fontLigatures" = true;
      "editor.formatOnSave" = true;

      # "astro.trace.server" = "verbose";

      # Controls when the folding controls on the gutter are shown.
      #  - always: Always show the folding controls.
      #  - never: Never show the folding controls and reduce the gutter size.
      #  - mouseover: Only show the folding controls when the mouse is over the gutter.
      "editor.showFoldingControls" = "never";

      # Controls diff decorations in the editor.
      #  - all: Show the diff decorations in all available locations.
      #  - gutter: Show the diff decorations only in the editor gutter.
      #  - overview: Show the diff decorations only in the overview ruler.
      #  - minimap: Show the diff decorations only in the minimap.
      #  - none: Do not show the diff decorations.
      "scm.diffDecorations" = "none";

      "debug.showInlineBreakpointCandidates" = false;
    };
  };
}
