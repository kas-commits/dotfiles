_: {
  programs.keychain = {
    enable = true;
    enableZshIntegration = true;
    inheritType = "any";
  };
}
