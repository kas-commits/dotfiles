{pkgs, ...}: {
  programs.nnn = {
    package = pkgs.nnn.override {withNerdIcons = true;};
    extraPackages = with pkgs; [ffmpegthumbnailer mediainfo];
    enable = true;
    bookmarks = {
      w = "~/Documents/coursework";
      d = "~/Documents/dev";
      D = "~/Downloads";
      b = "~/Documents/books";
    };
  };
}
