{pkgs, ...}: {
  programs.bat = {
    enable = true;
    config = {
      theme = "Catppuccin-macchiato";
    };
    themes = let
      catppuccin-repo = pkgs.fetchFromGitHub {
        owner = "catppuccin";
        repo = "bat";
        rev = "ba4d16880d63e656acced2b7d4e034e4a93f74b1";
        sha256 = "6WVKQErGdaqb++oaXnY3i6/GuH2FhTgK0v4TN4Y0Wbw=";
      };
    in {
      Catppuccin-macchiato = {
        src = catppuccin-repo;
        file = "Catppuccin-macchiato.tmTheme";
      };
      Catppuccin-latte = {
        src = catppuccin-repo;
        file = "Catppuccin-latte.tmTheme";
      };
      Catppuccin-frappe = {
        src = catppuccin-repo;
        file = "Catppuccin-frappe.tmTheme";
      };
      Catppuccin-mocha = {
        src = catppuccin-repo;
        file = "Catppuccin-mocha.tmTheme";
      };
    };
  };
}
