_: {
  programs.yazi = {
    enable = true;
    enableZshIntegration = true;
    settings = {
      manager = {
        sort_by = "modified";
        sort_sensitive = true;
        sort_reverse = true;
        sort_dir_first = true;
      };
    };
  };
}
