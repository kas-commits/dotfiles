{inputs, pkgs, ...}: {
  imports = [
    ./vscode
    ./helix
    # ./nnn
    ./zsh
    ./eza
    ./direnv
    ./zoxide
    ./zellij
    ./yazi
    ./starship
    ./fzf
    ./git
    ./gh
    ./bat
    ./go
    ./lazygit
    ./keychain
    ./kakoune
    ./lf
    ./nix
    ./neovim
  ];

  home.packages = let
    node_pkgs = pkgs.nodePackages;
    node = pkgs.nodejs_20;
  in
    with pkgs; [
      # CLI tools
      awscli2
      fd
      du-dust
      opentofu
      terraform
      ripgrep
      ninja
      jq
      xdg-ninja
      neofetch
      pdftk
      devbox
      just
      rclone
      trash-cli
      exiftool
      # termpdfpy
      wget
      tree-sitter
      chafa
      poppler_utils
      imagemagick
      yt-dlp
      node_pkgs.vercel
      trufflehog
      nh
      nvd
      nix-output-monitor
      btop
      linode-cli
      cacert
      inputs.agenix.packages.${pkgs.stdenv.system}.default
      sops

      # Services & Daemons
      sqlite
      postgresql_16
      kind

      # Formatters
      node_pkgs.prettier
      node_pkgs."@astrojs/language-server"

      # Language Servers
      nil
      terraform-ls
      lua-language-server
      node_pkgs.typescript-language-server
      node_pkgs.vscode-json-languageserver
      pyright
      texlab
      marksman
      amp
      taplo

      # TUI tools
      mprocs
      podman-tui

      # Languages
      node
      scala
      lua
      texlive.combined.scheme-full
      # R
      # julia-bin
      # jdk17

      # Containers
      podman
      podman-compose
      # kubernetes
      # kubernetes-helm

      ## Python
      # poetry
      # hatch
      pre-commit
      ruff
      ruff-lsp
      python3
      # (python312.withPackages (ps:
      #   with ps; [
      #     pip
      #     mkdocs
      #     mkdocs-material
      #     mkdocstrings
      #     mkdocstrings-python
      #     flit
      #     pandas
      #     hatchling
      #     psutil
      #   ]))

      # GUI Apps
      neovide

      # Additional utilities
      font-awesome
      (nerdfonts.override {
        fonts = [
          "RobotoMono"
          "JetBrainsMono"
          "FiraCode"
          "SourceCodePro"
        ];
      })
    ];
}
