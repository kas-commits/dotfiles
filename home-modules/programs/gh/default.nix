_: {
  programs.gh = {
    enable = true;
    settings = {
      version = "1";
      git_protocol = "ssh";
      prompt = "enabled";
      # editor = "hx";
      aliases = {
        co = "pr checkout";
        pv = "pr view";
      };
    };
  };
}
