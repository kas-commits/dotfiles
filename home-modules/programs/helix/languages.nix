{pkgs, ...}: {
  programs.helix.languages = {
    language-server = {
      pyright = {
        command = "pyright-langserver";
        args = ["--stdio"];
      };
      ruff-lsp = {
        command = "ruff-lsp";
      };
      astro-ls = {
        command = "astro-ls";
        args = ["--stdio"];
      };
      tailwindcss = {
        command = "${pkgs.tailwindcss-language-server}/bin/tailwindcss-language-server";
        args = ["--stdio"];
      };
    };

    language = [
      {
        name = "python";
        roots = ["pyproject.toml" "Poetry.lock" "setup.py"];
        indent = {
          tab-width = 4;
          unit = "    ";
        };
        auto-format = true;
        diagnostic-severity = "Hint"; # This is the default
        formatter = {
          command = "ruff";
          args = ["format" "--line-length" "79" "-"];
        };
        language-servers = [
          {
            name = "pyright";
            except-features = ["format"];
          }
          {
            name = "ruff-lsp";
            # except-features = [ "format" ];
          }
        ];
      }
      {
        name = "c-sharp";
        auto-format = true;
        diagnostic-severity = "Hint"; # This is the default
      }
      {
        name = "ocaml";
        auto-format = true;
        diagnostic-severity = "Hint"; # This is the default
      }
      {
        name = "typescript";
        auto-format = true;
        language-servers = [
          {
            name = "vscode-eslint-language-server";
          }
          {
            name = "typescript-language-server";
          }
        ];
      }
      {
        name = "astro";
        auto-format = true;
        diagnostic-severity = "Hint"; # This is the default
        roots = ["astro.config.mjs" "tsconfig.json" "package.json"];
        language-servers = [
          {
            name = "tailwindcss";
          }
          {
            name = "astro-ls";
          }
        ];
      }
      # {
      #   name = "markdown";
      #   auto-format = true;
      #   formatter = {
      #     command = "prettier";
      #     args = [ "--parser" "markdown" "--prose-wrap" "always" "--print-width" "80" ];
      #   };
      # }
    ];
  };
}
