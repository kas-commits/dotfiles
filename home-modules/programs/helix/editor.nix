_: {
  programs.helix.settings.editor = {
    line-number = "relative";
    cursorline = false;
    mouse = true;
    color-modes = true;
    true-color = true;
    gutters = ["diff"];
    auto-save = true;
    auto-format = true;
    auto-completion = true; # Enable automatic pop up of auto-completion
    preview-completion-insert = true; # Apply completion when selected
    completion-trigger-len = 2; # length of word to trigger autocompletion
    completion-replace = false; # replace entire word not just before the cursor
    undercurl = true;
    idle-timeout = 50;
    bufferline = "always"; # always, never, or multiple
    insert-final-newline = true;
    shell = ["sh" "-c"];
    scrolloff = 4;
    rulers = [80];
    indent-guides.render = true;
    soft-wrap = {
      enable = true;
      wrap-at-text-width = true;
    };
    whitespace = {
      render = {
        space = "none";
        tab = "all";
        newline = "all";
        nbsp = "all";
      };
      characters = {
        space = "·";
        nbsp = "⍽";
        tab = "→";
        newline = "⏎";
      };
    };
    lsp = {
      display-messages = true;
      display-inlay-hints = false;
      goto-reference-include-declaration = false;
      display-signature-help-docs = true;
      auto-signature-help = true;
    };
    cursor-shape = {
      # insert = "bar";
      insert = "block";
      normal = "block";
      select = "block";
    };
    file-picker = {
      hidden = false;
      parents = true;
      ignore = true;
      git-ignore = true;
      git-global = true;
      git-exclude = true;
    };
  };
}
