{pkgs}:
pkgs.rustPlatform.buildRustPackage rec {
  pname = "helix";
  version = "24.07";

  # this allows you to escape the sandbox and hence build the grammars
  __noChroot = true;

  src = pkgs.fetchFromGitHub {
    owner = "helix-editor";
    repo = pname;
    rev = "b4811f7d2e117532f7e4b60e8cf1088d283c37f5";
    hash = "sha256-ZZ9AZls8Whht0zo72EdMOxDv9U86SEV8bCut7GllBno=";
  };

  cargoHash = "sha256-3RCDNISx7VsSfK8DNJCKh0o3EPUi9wsogXqKtT4Cdt4=";

  nativeBuildInputs = [
    pkgs.git
    pkgs.installShellFiles
    pkgs.makeWrapper
    pkgs.cacert
  ];

  env.HELIX_DEFAULT_RUNTIME = "${placeholder "out"}/lib/runtime";

  postInstall = ''
    # not needed at runtime
    rm -r runtime/grammars/sources

    mkdir -p $out/lib
    cp -r runtime $out/lib
    installShellCompletion contrib/completion/hx.{bash,fish,zsh}
    mkdir -p $out/share/{applications,icons/hicolor/256x256/apps}
    cp contrib/Helix.desktop $out/share/applications
    cp contrib/helix.png $out/share/icons/hicolor/256x256/apps
  '';
}
