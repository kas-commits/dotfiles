{pkgs, ...}: {
  imports = [
    # ./theme.nix
    ./languages.nix
    ./editor.nix
  ];

  programs.helix = {
    # # Uncomment if you want a custom build of Helix.
    package = import ./custompkg.nix {inherit pkgs;};
    # package = pkgs.helix;

    enable = true;
    defaultEditor = true;
    settings = {
      # theme = "catppuccin_mocha";
      theme = "kanagawa";
      keys = {
        normal = {
          A-q = ":write-quit-all";
          A-e = ":buffer-close-others";
          A-w = ":buffer-close";
          A-s = ":update";
        };
      };
    };
  };
}
