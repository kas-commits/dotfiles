_: {
  programs.zellij = {
    enable = true;
    enableZshIntegration = false;
    settings = {
      default_layout = "compact";
      theme = "catppuccin-macchiato";
      default_mode = "locked";
      pane_frames = false;
      scroll_buffer_size = 0;
      mouse_mode = false;
      copy_clipboard = "system"; # can also be "primary"
      copy_on_select = true;
      ui = {
        pane_frames = {
          rounded_corners = true;
          hide_session_name = true;
        };
      };
    };
  };
}
