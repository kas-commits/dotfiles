_: {
  programs.git = {
    enable = true;
    userName = "Karem Abdul-Samad";
    userEmail = "kas2020@protonmail.com";
    extraConfig = {
      init.defaultBranch = "master";
    };
    delta = {enable = true;};
    ignores = [
      "*~"
      "*.swp"
      ".DS_Store"
    ];
  };
}
