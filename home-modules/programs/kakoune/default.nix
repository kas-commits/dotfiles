_: {
  programs.kakoune = {
    enable = true;

    # set global lsp_cmd "kak-lsp -s %val{session} -vvv --log /tmp/kak-lsp.log"
    extraConfig = ''
      eval %sh{kak-lsp --kakoune -s $kak_session}  # Not needed if you load it with plug.kak.

      hook global WinSetOption filetype=(python|javascript|typescript|c|cpp|nix) %{
        lsp-enable-window
      }

      # Automatically format code on write using the lsp server
      hook global WinSetOption filetype=python %{
        hook window BufWritePre .* lsp-formatting-sync
      }

      # These hooks make sure the git info is refreshed
      hook global WinCreate .* %{ git show-diff }
      hook global BufWritePost .* %{ git update-diff }
      hook global BufReload .* %{ git update-diff }
    '';
    config = {
      colorScheme = "kanagawa";
      indentWidth = 2;
      keyMappings = [
        {
          key = "<a-s>";
          effect = ":write<ret>";
          mode = "normal";
          docstring = "shortcut to write";
        }
        {
          key = "<a-q>";
          effect = ":quit<ret>";
          mode = "normal";
          docstring = "shortcut to quit";
        }
        {
          key = "<c-a>";
          effect = ":comment-line<ret>";
          mode = "normal";
          docstring = "shortcut to comment line";
        }
        {
          key = "l";
          effect = ":enter-user-mode lsp<ret>";
          mode = "user";
          docstring = "enter lsp mode";
        }
        {
          key = "g";
          effect = ":enter-user-mode git<ret>";
          mode = "user";
          docstring = "enter git mode";
        }
        {
          key = "b";
          effect = ":git blame<ret>";
          mode = "git";
          docstring = "show git blame output on the side";
        }
        {
          key = "h";
          effect = ":git hide-blame<ret>";
          mode = "git";
          docstring = "hide git blame output";
        }
        {
          key = "n";
          effect = ":git next-hunk<ret>";
          mode = "git";
          docstring = "jump cursor to the next hunk";
        }
        {
          key = "p";
          effect = ":git prev-hunk<ret>";
          mode = "git";
          docstring = "jump cursor to the prev hunk";
        }
        {
          key = "<c-w>";
          effect = ":delete-buffer<ret>";
          mode = "normal";
          docstring = "shortcut to delete buffer";
        }
        {
          key = "<a-backspace>";
          effect = "<esc>bdi";
          mode = "insert";
          docstring = "shortcut to delete buffer";
        }
        {
          key = "<a-n>";
          effect = "<tab>";
          mode = "prompt";
        }
        {
          key = "<a-p>";
          effect = "<s-tab>";
          mode = "prompt";
        }
      ];
      numberLines = {
        enable = true;
        highlightCursor = true;
        relative = true;
      };
      scrollOff = {
        columns = 0;
        lines = 4;
      };
      showMatching = true;
      showWhitespace = {
        enable = true;
        space = " ";
      };
      wrapLines = {
        enable = true;
        marker = "⏎";
        maxWidth = 120;
        word = true;
      };
      ui = {
        setTitle = false;
        assistant = "clippy";
        statusLine = "top"; # also can be "bottom"
      };
    };
  };
}
