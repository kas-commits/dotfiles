{
  lib,
  pkgs,
  ...
}: {
  programs.starship = {
    enable = true;
    enableZshIntegration = true;
    settings = let
      flavour = "mocha"; # `latte` | `frappe` | `macchiato` | `mocha`
    in
      {
        palette = "catppuccin_${flavour}";
        continuation_prompt = "▶▶ ";
        # you can also add right_format for things on the right side
        format = lib.concatStrings [
          "$directory"
          "$git_branch"
          "$git_commit"
          "$git_state"
          "$git_status"
          "$jobs"
          "$python"
          "$nodejs"
          "$lua"
          "$terraform"
          "$nix_shell"
          "$status"
          "$line_break"
          "$character"
        ];
        add_newline = true;
        shlvl = {
          symbol = "↕ "; # 
          disabled = false;
        };
        lua = {
          symbol = " ";
          # format = "[$symbol($version)]($style)";
        };
        rust.symbol = " ";
        python = {
          symbol = " ";
          # format = "[$symbol$version ($virtualenv)]($style)";
        };
        terraform = {
          # format = " [$symbol$workspace]($style)";
        };
        nodejs = {
          disabled = true;
          # symbol = " ";
          # format = "[$symbol($version)]($style)";
        };
        git_branch = {
          # format = "[$symbol$branch(:$remote_branch)]($style) ";
        };
        git_status = {
          conflicted = "=";
          ahead = "⇡";
          behind = "⇣";
          diverged = "⇕";
          untracked = "?";
          stashed = "S";
          modified = "!";
          staged = "+";
          renamed = "»";
          deleted = "✘";
          style = "bold red";
          disabled = false;
        };
        line_break.disabled = false;
        directory = {
          truncation_length = 2;
          style = "fg:blue";
          format = "[$path]($style)[$read_only]($read_only_style) ";
        };
        character = {
          format = "$symbol";
          success_symbol = "[λ ](green)"; # λ ➜  ❯ 
          error_symbol = "[λ ](red)"; # ✖ ✗ ❯
          vicmd_symbol = "[ ](green)";
        };
        status = {
          disabled = false;
          format = "[($common_meaning )$status]($style) ";
        };
      }
      // builtins.fromTOML (builtins.readFile
        (pkgs.fetchFromGitHub
          {
            owner = "catppuccin";
            repo = "starship";
            rev = "5629d2356f62a9f2f8efad3ff37476c19969bd4f"; # Replace with the latest commit hash
            sha256 = "nsRuxQFKbQkyEI4TXgvAjcroVdG+heKX5Pauq/4Ota0=";
          }
          + /palettes/${flavour}.toml));
  };
}
