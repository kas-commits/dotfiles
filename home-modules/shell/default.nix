_: {
  home = let
    XDG_DATA_HOME = "$HOME/.local/share";
    XDG_CONFIG_HOME = "$HOME/.config";
    XDG_STATE_HOME = "$HOME/.local/state";
    # XDG_CACHE_HOME = "$HOME/.cache";
  in {
    sessionVariables = {
      AWS_SHARED_CREDENTIALS_FILE = "${XDG_CONFIG_HOME}/aws/credentials";
      AWS_CONFIG_FILE = "${XDG_CONFIG_HOME}/aws/config";
      CABAL_CONFIG = "${XDG_CONFIG_HOME}/cabal/config";
      CABAL_DIR = "${XDG_DATA_HOME}/cabal";
      # CARGO_HOME = "${XDG_DATA_HOME}/cargo";
      GOPATH = "${XDG_DATA_HOME}/go";
      DOCKER_CONFIG = "${XDG_CONFIG_HOME}/docker";
      RUSTUP_HOME = "${XDG_DATA_HOME}/rustup";
      KERAS_HOME = "${XDG_STATE_HOME}/keras";
      PSQL_HISTORY = "${XDG_DATA_HOME}/psql_history";
      IPYTHONDIR = "${XDG_CONFIG_HOME}/ipython";
      MINIKUBE_HOME = "${XDG_DATA_HOME}/minikube";
      HISTFILE = "${XDG_STATE_HOME}/.zsh_history";
      JULIA_DEPOT_PATH = "${XDG_DATA_HOME}/julia:$JULIA_DEPOT_PATH";
      GHCUP_USE_XDG_DIRS = "true";
      READER = "zathura";
      FILE = "nnn";
      TERMINAL = "kitty";
      TERMALT = "alacritty";
      TERM = "xterm";
      VIDEO = "mpv";
      COLORTERM = "truecolor";
      OPENER = "xdg-open";
      # BAT_THEME = "Catppuccin-macchiato";
      BREW_HOME = "/opt/homebrew";
      NNN_USE_EDITOR = 1;
      NIXPKGS_ALLOW_UNFREE = 1;
      FLAKE = "$HOME/dev/dotfiles";
    };

    sessionPath = [
      "$BREW_HOME/bin"
      "$HOME/.local/bin"
    ];

    shellAliases = {
      lg = "lazygit";
      nd = "nom develop --impure -c $SHELL";
      hms = "nh home switch";
      # hms = "home-manager switch --flake .";
      # hms = ''nix build --impure ".#homeConfigurations.$USER@$(hostname).activationPackage" && ./result/activate && unlink result'';
      wget = "wget --hsts-file=$XDG_DATA_HOME/wget-hsts";
      cfix = "echo -ne \"\\e[4 q\"";
      kakf = ''kak "$(fd -t f .| fzf --exit-0)"'';
      cdf = ''cd "$(fd -t d . | fzf --exit-0)"'';
    };
  };
}
